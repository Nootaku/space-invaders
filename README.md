# Discovering Rust - Space Invaders

[![Developer](https://img.shields.io/badge/Developed_by-Nootaku_(MWattez)-informational?style=for-the-badge)](https://gitlab.com/Nootaku)<br/>
![Version](https://img.shields.io/badge/Version-1.0.0-yellow?style=for-the-badge)


Terminal based [Space Invaders](https://freeinvaders.org/) rip-off.<br/>
The aim of this project is to get hands-on experience with the Rust programming language.

More specifically, to gain experience with a more complex project and imports of crates.

## How to play

Run the program with:
```bash
cargo run
```

These are the following hot-keys for the game:
- `f` or `left` to move left
- `j` or `right` to move right
- `space` to shoot
- `escape` to quit