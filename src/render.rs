use std::io::Stdout;
use std::io::Write;
use crate::frame::Frame;
use crossterm::QueueableCommand;
use crossterm::style::{SetBackgroundColor, Color};
use crossterm::terminal::{Clear, ClearType};
use crossterm::cursor::MoveTo;

pub fn render(stdout: &mut Stdout, last_frame: &Frame, current_frame: &Frame, force: bool) {
    // Initialization
    if force {
        stdout.queue(SetBackgroundColor(Color::Blue)).unwrap();
        stdout.queue(Clear(ClearType::All)).unwrap();
        stdout.queue(SetBackgroundColor(Color::Black)).unwrap();
    }

    // Print whatever is in the frame vector
    // Iterate over the vector (enumerate gets 'x' index)
    for (x_index, column) in current_frame.iter().enumerate() {
        // Iterate over each column (enumerate gets 'y' index)
        for (y_index, string) in column.iter().enumerate() {
            
            // Compare the string to the last frame
            if *string != last_frame[x_index][y_index] || force {
                stdout.queue(MoveTo(x_index as u16, y_index as u16)).unwrap();
                print!("{}", *string);
            }
        }
    }

    // cleanup
    stdout.flush().unwrap();
}
