use std::{
    error::Error,
    io,
    sync::mpsc,
    time::{Duration, Instant},
    thread,
};
use crossterm::{
    terminal::{self, EnterAlternateScreen, LeaveAlternateScreen},
    cursor::{Hide, Show},
    event::{self, Event, KeyCode},
    ExecutableCommand
};
use rusty_audio::Audio;

// Local imports
use space_invaders::{
    frame::{self, Drawable, new_frame},
    render,
    player::Player,
    invader::Invaders,
};


fn main() -> Result <(), Box<dyn Error>>{

    // Load the sounds
    let mut audio = Audio::new();
    audio.add("startup", "audio/startup.mp3");
    audio.add("win", "audio/win.mp3");
    audio.add("lose", "audio/lose.mp3");
    audio.add("pew", "audio/pew.wav");
    audio.add("move", "audio/move.wav");
    audio.add("explode", "audio/explode.wav");

    audio.play("startup");

    // Setup Terminal access
    // Documentation:
    //     - https://doc.rust-lang.org/std/io/struct.Stdout.html
    //     - https://docs.rs/crossterm/latest/crossterm/terminal/index.html
    let mut stdout = io::stdout();  // gives access to terminal
    terminal::enable_raw_mode()?;  // allows live input
    stdout.execute(EnterAlternateScreen)?; // create an alternate screen (just like in VIM)
    stdout.execute(Hide)?; // Hide cursor
    
    // Render loop (in separate thread)
    let (render_tx, render_rx) = mpsc::channel();
    let render_handle = thread::spawn(move || {
        let mut last_frame = frame::new_frame();
        let mut stdout = io::stdout();
        render::render(&mut stdout, &last_frame, &last_frame, true);

        loop {
            let current_frame = match render_rx.recv() {
                Ok(x) => x,
                Err(_) => break,
            };

            render::render(&mut stdout, &last_frame, &current_frame, false);
            last_frame = current_frame;
        }
    });

    // Game Loop ----------------------------------------------------------
    let mut player = Player::new();
    let mut instant = Instant::now();
    let mut invaders = Invaders::new();

    // Name the loop "gameloop"
    'gameloop: loop {
        // Frame initialization
        let delta = instant.elapsed();
        instant = Instant::now();
        let mut current_frame = new_frame();

        // Input
        while event::poll(Duration::default())? {
            // Watch for specific key inputs
            if let Event::Key(key_event) = event::read()? {
                match key_event.code {

                    // Exit game if 'esc' or 'q'
                    KeyCode::Esc | KeyCode::Char('q') => {
                        audio.play("lose");
                        break 'gameloop;
                    },
                    KeyCode::Left | KeyCode::Char('f') => player.move_left(),
                    KeyCode::Right | KeyCode::Char('j') => player.move_right(),
                    KeyCode::Char(' ') | KeyCode::Enter => {
                        if player.shoot() {
                            audio.play("pew");
                        }
                    },

                    _ => {}
                }
            }
        }

        // Updates and Timers
        player.update(delta);
        if invaders.update(delta) {
            audio.play("move");
        }
        if player.detect_hits(&mut invaders) {
            audio.play("explode");
        }

        // Draw and render
        let drawables: Vec<&dyn Drawable> = vec![&player, &invaders];
        for drawable in drawables {
            drawable.draw(&mut current_frame);
        }
        let _ = render_tx.send(current_frame);
        thread::sleep(Duration::from_millis(1));


        // Win or Lose
        if invaders.all_killed() {
            audio.play("win");
            break 'gameloop;
        }
        if invaders.reached_bottom() {
            audio.play("lose");
            break 'gameloop;
        }
    }

    // cleanup
    drop(render_tx); // to ensure that the thread is stopped
    render_handle.join().unwrap();
    audio.wait();
    stdout.execute(Show)?;
    stdout.execute(LeaveAlternateScreen)?;
    terminal::disable_raw_mode()?;
    Ok(())
}
