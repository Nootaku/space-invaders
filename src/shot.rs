use std::time::Duration;
use rusty_time::Timer;
use crate::{
	frame::{Drawable, Frame},
};

// File contains the logic of shots
// This structure is interesting because it clearly shows when a mutable
// version is necessary and when it isn't.
pub struct Shot {
	pub x: usize,
	pub y: usize,
	pub exploding: bool,
	timer: Timer,
}


impl Shot {
	pub fn new(x: usize, y: usize) -> Self {
		Self {
			x: x,
			y: y - 1,
			exploding: false,
			timer: Timer::new(Duration::from_millis(50)),
		}
	}

	pub fn update(&mut self, delta: Duration) {
		self.timer.tick(delta);

		if self.timer.finished() && !self.exploding {
			if self.y > 0 {
				self.y -= 1;
			}
			self.timer.reset();
		}
	}

	pub fn explode(&mut self) {
		self.exploding = true;
		self.timer = Timer::new(Duration::from_millis(250));
	}

	pub fn dead(&self) -> bool {
		// We need read-only so no mutable self
		let has_exploded: bool = self.exploding && self.timer.finished();
		let has_reached_top: bool = self.y == 0;

		has_exploded || has_reached_top
	}
}


impl Drawable for Shot {
	fn draw(&self, frame: &mut Frame) {
		frame[self.x][self.y] = if self.exploding {"*"} else {"|"};
	}
}